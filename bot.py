import tweepy
import config

print("Bot is running")

#This is that auth required to give the bot access to the API
auth = tweepy.OAuth1UserHandler(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret
)

api = tweepy.API(auth)

#input the id of the twitter account that will be acting as a bot as an int
bot_id = 1
#write tha name of your bot including the @ here. This variable will be used in a few places. 
bot_name = "@rs2190"

def tweeting_function():
    #The message that will become a tweet
    print("Sending Tweet 😏")
    message = "Hey! This is my first tweet."
    api.update_status(status = message)
    print("Tweet Successful 🥳")

tweeting_function()


def commenting_function(twt):
    #the id of the tweet that activated the function
    the_id = twt.id
    #the name of the author of the above tweet
    name = twt.author.screen_name
    #This will be the tweet that gets sent back to the author
    message = f"@{name} This is a reply tweet"

    print("Tweet Found in stream!")
    #twt.author.screen_name, etc, are taken from Twitter docs LINK HERE
    #PRinting the name and text of the tweet found in stream. 
    print(f"TWEET: {name} - {twt.text}")
    #checks to make sure the tweet is not from the bot itself, that it is not a quote tweet and that it is not a retweet. 
    if ((twt.author.id != bot_id) and not (hasattr(twt, "quoted_status")) and not (hasattr(twt, "retweeted_status"))):
        #twitter used to have shorter tweets. The new longer tweet is called an extended tweet. This try except checks to see if twt.extended_tweet has an attribute of "full_text". If it doesn't, the_tweet is twt.text. This is checked for containing the words in the if else ahead. 
        try:
            the_tweet = twt.extended_tweet["full_text"]
        except AttributeError:
            the_tweet = twt.text
        #tweet has "please" and the bot's name in it
        if ("please" in the_tweet) and (bot_name in the_tweet):
            #another try/except to see if we're able to comment. If not, an error is printed to the console.
            try:
                print("Attempting Comment")
                api.update_status(status = message, in_reply_to_status_id = the_id)
                print("TWEET SUCCESSFUL")
            except Exception as err:
                print(err)
        #else for whether the tweet has `please` in it and if the bot was tagged in the tweet.
        else:
            print("Tweet wasn't nice enough")
    #else statement for the logic that keeps this from firing on retweets, quote tweets, and posts by the bot itself. 
    else:
        print("Could not comment: RT, QT, or bot tweet")


#a subclass is required for a [Tweepy&Twitter stream](https://docs.tweepy.org/en/stable/streaming.html). We're creating a subclass of Stream called `MyStreamListener` with a parameter of tweepy.Stream. tweepy.Stream requires four keys to be passed in much like `auth` above. This subclass includes the function on_status which runs our commenting_function function.
class MyStreamListener(tweepy.Stream):
#on_status is what happens when a status ("tweet") comes into the stream
    def on_status(self, twts):
        commenting_function(twts)

#create instance of stream class and pass in the keys. 
stream_m = MyStreamListener(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret)

#use "follow" to RT a specific account (for example: if you want to RT only tweets from a specific account, use `follow`), use "track" for tweets that contain the string included here. Note that track must be an array. This begins the stream with the filter looking specifically at interactions with the bot listed in the list for `track`
stream_m.filter(track =[bot_name])